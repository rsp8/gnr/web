# General

We put projects that are needed in multiple other
projects here. So instead of repeating the simulation
theory or manufacturing process in every other project,
we put them here.

We also keep meta projects and projects that don't fit in
with others here.
